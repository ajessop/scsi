# Pin name action command file

# Start of element R208
ChangePinName(R208, 1, 1)
ChangePinName(R208, 2, 2)

# Start of element R207
ChangePinName(R207, 1, 1)
ChangePinName(R207, 2, 2)

# Start of element R206
ChangePinName(R206, 1, 1)
ChangePinName(R206, 2, 2)

# Start of element R205
ChangePinName(R205, 1, 1)
ChangePinName(R205, 2, 2)

# Start of element Q202
ChangePinName(Q202, E, E)
ChangePinName(Q202, C, C)
ChangePinName(Q202, B, B)

# Start of element R204
ChangePinName(R204, 1, 1)
ChangePinName(R204, 2, 2)

# Start of element R203
ChangePinName(R203, 1, 1)
ChangePinName(R203, 2, 2)

# Start of element Q201
ChangePinName(Q201, S, S)
ChangePinName(Q201, D, D)
ChangePinName(Q201, G, G)

# Start of element R202
ChangePinName(R202, 1, 1)
ChangePinName(R202, 2, 2)

# Start of element R201
ChangePinName(R201, 1, 1)
ChangePinName(R201, 2, 2)

# Start of element D201
ChangePinName(D201, 2, 2)
ChangePinName(D201, 1, 1)

# Start of element J5
ChangePinName(J5, 20, 20)
ChangePinName(J5, 19, 19)
ChangePinName(J5, 18, 18)
ChangePinName(J5, 17, 17)
ChangePinName(J5, 16, 16)
ChangePinName(J5, 15, 15)
ChangePinName(J5, 14, 14)
ChangePinName(J5, 13, 13)
ChangePinName(J5, 12, 12)
ChangePinName(J5, 11, 11)
ChangePinName(J5, 10, 10)
ChangePinName(J5, 9, 9)
ChangePinName(J5, 8, 8)
ChangePinName(J5, 7, 7)
ChangePinName(J5, 6, 6)
ChangePinName(J5, 5, 5)
ChangePinName(J5, 4, 4)
ChangePinName(J5, 3, 3)
ChangePinName(J5, 2, 2)
ChangePinName(J5, 1, 1)

# Start of element J4
ChangePinName(J4, 50, 50)
ChangePinName(J4, 49, 49)
ChangePinName(J4, 48, 48)
ChangePinName(J4, 47, 47)
ChangePinName(J4, 46, 46)
ChangePinName(J4, 45, 45)
ChangePinName(J4, 44, 44)
ChangePinName(J4, 43, 43)
ChangePinName(J4, 42, 42)
ChangePinName(J4, 41, 41)
ChangePinName(J4, 40, 40)
ChangePinName(J4, 39, 39)
ChangePinName(J4, 38, 38)
ChangePinName(J4, 37, 37)
ChangePinName(J4, 36, 36)
ChangePinName(J4, 35, 35)
ChangePinName(J4, 34, 34)
ChangePinName(J4, 33, 33)
ChangePinName(J4, 32, 32)
ChangePinName(J4, 31, 31)
ChangePinName(J4, 30, 30)
ChangePinName(J4, 29, 29)
ChangePinName(J4, 28, 28)
ChangePinName(J4, 27, 27)
ChangePinName(J4, 26, 26)
ChangePinName(J4, 25, 25)
ChangePinName(J4, 24, 24)
ChangePinName(J4, 23, 23)
ChangePinName(J4, 22, 22)
ChangePinName(J4, 21, 21)
ChangePinName(J4, 20, 20)
ChangePinName(J4, 19, 19)
ChangePinName(J4, 18, 18)
ChangePinName(J4, 17, 17)
ChangePinName(J4, 16, 16)
ChangePinName(J4, 15, 15)
ChangePinName(J4, 14, 14)
ChangePinName(J4, 13, 13)
ChangePinName(J4, 12, 12)
ChangePinName(J4, 11, 11)
ChangePinName(J4, 10, 10)
ChangePinName(J4, 9, 9)
ChangePinName(J4, 8, 8)
ChangePinName(J4, 7, 7)
ChangePinName(J4, 6, 6)
ChangePinName(J4, 5, 5)
ChangePinName(J4, 4, 4)
ChangePinName(J4, 3, 3)
ChangePinName(J4, 2, 2)
ChangePinName(J4, 1, 1)

# Start of element C15
ChangePinName(C15, 2, 2)
ChangePinName(C15, 1, 1)

# Start of element C14
ChangePinName(C14, 2, 2)
ChangePinName(C14, 1, 1)

# Start of element C13
ChangePinName(C13, 2, 2)
ChangePinName(C13, 1, 1)

# Start of element C12
ChangePinName(C12, 2, 2)
ChangePinName(C12, 1, 1)

# Start of element C11
ChangePinName(C11, 2, 2)
ChangePinName(C11, 1, 1)

# Start of element C10
ChangePinName(C10, 2, 2)
ChangePinName(C10, 1, 1)

# Start of element C9
ChangePinName(C9, 2, 2)
ChangePinName(C9, 1, 1)

# Start of element C8
ChangePinName(C8, 2, 2)
ChangePinName(C8, 1, 1)

# Start of element C7
ChangePinName(C7, 2, 2)
ChangePinName(C7, 1, 1)

# Start of element C6
ChangePinName(C6, 2, 2)
ChangePinName(C6, 1, 1)

# Start of element C5
ChangePinName(C5, 2, 2)
ChangePinName(C5, 1, 1)

# Start of element C4
ChangePinName(C4, 2, 2)
ChangePinName(C4, 1, 1)

# Start of element C3
ChangePinName(C3, 2, 2)
ChangePinName(C3, 1, 1)

# Start of element C2
ChangePinName(C2, 2, 2)
ChangePinName(C2, 1, 1)

# Start of element C1
ChangePinName(C1, 2, 2)
ChangePinName(C1, 1, 1)

# Start of element U15
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, 11, A)
ChangePinName(U15, 12, B)
ChangePinName(U15, 13, Y)
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, 8, A)
ChangePinName(U15, 9, B)
ChangePinName(U15, 10, Y)
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, 5, A)
ChangePinName(U15, 6, B)
ChangePinName(U15, 4, Y)
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, unknown, unknown)
ChangePinName(U15, 2, A)
ChangePinName(U15, 3, B)
ChangePinName(U15, 1, Y)

# Start of element U14
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, 11, A)
ChangePinName(U14, 12, B)
ChangePinName(U14, 13, Y)
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, 8, A)
ChangePinName(U14, 9, B)
ChangePinName(U14, 10, Y)
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, 5, A)
ChangePinName(U14, 6, B)
ChangePinName(U14, 4, Y)
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, unknown, unknown)
ChangePinName(U14, 2, A)
ChangePinName(U14, 3, B)
ChangePinName(U14, 1, Y)

# Start of element U13
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, 11, A)
ChangePinName(U13, 12, B)
ChangePinName(U13, 13, Y)
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, 8, A)
ChangePinName(U13, 9, B)
ChangePinName(U13, 10, Y)
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, 5, A)
ChangePinName(U13, 6, B)
ChangePinName(U13, 4, Y)
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, unknown, unknown)
ChangePinName(U13, 2, A)
ChangePinName(U13, 3, B)
ChangePinName(U13, 1, Y)

# Start of element U12
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, 11, A)
ChangePinName(U12, 12, B)
ChangePinName(U12, 13, Y)
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, 8, A)
ChangePinName(U12, 9, B)
ChangePinName(U12, 10, Y)
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, 5, A)
ChangePinName(U12, 6, B)
ChangePinName(U12, 4, Y)
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, unknown, unknown)
ChangePinName(U12, 2, A)
ChangePinName(U12, 3, B)
ChangePinName(U12, 1, Y)

# Start of element U11
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, 11, A)
ChangePinName(U11, 12, B)
ChangePinName(U11, 13, Y)
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, 8, A)
ChangePinName(U11, 9, B)
ChangePinName(U11, 10, Y)
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, 5, A)
ChangePinName(U11, 6, B)
ChangePinName(U11, 4, Y)
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, unknown, unknown)
ChangePinName(U11, 2, A)
ChangePinName(U11, 3, B)
ChangePinName(U11, 1, Y)

# Start of element U10
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, 10, \_EN\_)
ChangePinName(U10, 8, Y)
ChangePinName(U10, 9, A)
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, 13, \_EN\_)
ChangePinName(U10, 11, Y)
ChangePinName(U10, 12, A)
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, 4, \_EN\_)
ChangePinName(U10, 6, Y)
ChangePinName(U10, 5, A)
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, unknown, unknown)
ChangePinName(U10, 1, \_EN\_)
ChangePinName(U10, 3, Y)
ChangePinName(U10, 2, A)

# Start of element U9
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, 13, \_EN\_)
ChangePinName(U9, 11, Y)
ChangePinName(U9, 12, A)
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, 10, \_EN\_)
ChangePinName(U9, 8, Y)
ChangePinName(U9, 9, A)
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, 4, \_EN\_)
ChangePinName(U9, 6, Y)
ChangePinName(U9, 5, A)
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, unknown, unknown)
ChangePinName(U9, 1, \_EN\_)
ChangePinName(U9, 3, Y)
ChangePinName(U9, 2, A)

# Start of element U8
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, 13, \_EN\_)
ChangePinName(U8, 11, Y)
ChangePinName(U8, 12, A)
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, 10, \_EN\_)
ChangePinName(U8, 8, Y)
ChangePinName(U8, 9, A)
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, 4, \_EN\_)
ChangePinName(U8, 6, Y)
ChangePinName(U8, 5, A)
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, unknown, unknown)
ChangePinName(U8, 1, \_EN\_)
ChangePinName(U8, 3, Y)
ChangePinName(U8, 2, A)

# Start of element U7
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, 13, \_EN\_)
ChangePinName(U7, 11, Y)
ChangePinName(U7, 12, A)
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, 10, \_EN\_)
ChangePinName(U7, 8, Y)
ChangePinName(U7, 9, A)
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, 4, \_EN\_)
ChangePinName(U7, 6, Y)
ChangePinName(U7, 5, A)
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, unknown, unknown)
ChangePinName(U7, 1, \_EN\_)
ChangePinName(U7, 3, Y)
ChangePinName(U7, 2, A)

# Start of element U6
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, 13, \_EN\_)
ChangePinName(U6, 11, Y)
ChangePinName(U6, 12, A)
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, 10, \_EN\_)
ChangePinName(U6, 8, Y)
ChangePinName(U6, 9, A)
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, 4, \_EN\_)
ChangePinName(U6, 6, Y)
ChangePinName(U6, 5, A)
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, unknown, unknown)
ChangePinName(U6, 1, \_EN\_)
ChangePinName(U6, 3, Y)
ChangePinName(U6, 2, A)

# Start of element U5
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, 8, Y)
ChangePinName(U5, 9, A)
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, 6, Y)
ChangePinName(U5, 5, A)
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, 4, Y)
ChangePinName(U5, 3, A)
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, unknown, unknown)
ChangePinName(U5, 2, Y)
ChangePinName(U5, 1, A)

# Start of element U4
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, 8, Y)
ChangePinName(U4, 9, A)
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, 6, Y)
ChangePinName(U4, 5, A)
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, 4, Y)
ChangePinName(U4, 3, A)
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, unknown, unknown)
ChangePinName(U4, 2, Y)
ChangePinName(U4, 1, A)

# Start of element U3
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, 8, Y)
ChangePinName(U3, 9, A)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, 6, Y)
ChangePinName(U3, 5, A)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, 4, Y)
ChangePinName(U3, 3, A)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, unknown, unknown)
ChangePinName(U3, 2, Y)
ChangePinName(U3, 1, A)

# Start of element U2
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, 8, Y)
ChangePinName(U2, 9, A)
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, 6, Y)
ChangePinName(U2, 5, A)
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, 4, Y)
ChangePinName(U2, 3, A)
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, unknown, unknown)
ChangePinName(U2, 2, Y)
ChangePinName(U2, 1, A)

# Start of element U1
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, 8, Y)
ChangePinName(U1, 9, A)
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, 6, Y)
ChangePinName(U1, 5, A)
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, 4, Y)
ChangePinName(U1, 3, A)
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, unknown, unknown)
ChangePinName(U1, 2, Y)
ChangePinName(U1, 1, A)

# Start of element R106
ChangePinName(R106, 1, 1)
ChangePinName(R106, 2, 2)

# Start of element R105
ChangePinName(R105, 1, 1)
ChangePinName(R105, 2, 2)

# Start of element R104
ChangePinName(R104, 1, 1)
ChangePinName(R104, 2, 2)

# Start of element CONN3
ChangePinName(CONN3, 5, 5)
ChangePinName(CONN3, 3, 3)
ChangePinName(CONN3, 1, 1)
ChangePinName(CONN3, 6, 6)
ChangePinName(CONN3, 4, 4)
ChangePinName(CONN3, 2, 2)

# Start of element U102
ChangePinName(U102, 9, 9)
ChangePinName(U102, 10, 10)
ChangePinName(U102, 11, 11)
ChangePinName(U102, 12, 12)
ChangePinName(U102, 13, 13)
ChangePinName(U102, 14, 14)
ChangePinName(U102, 15, 15)
ChangePinName(U102, 16, 16)
ChangePinName(U102, 8, 8)
ChangePinName(U102, 7, 7)
ChangePinName(U102, 6, 6)
ChangePinName(U102, 5, 5)
ChangePinName(U102, 4, 4)
ChangePinName(U102, 3, 3)
ChangePinName(U102, 2, 2)
ChangePinName(U102, 1, 1)

# Start of element R103
ChangePinName(R103, 1, 1)
ChangePinName(R103, 2, 2)

# Start of element R102
ChangePinName(R102, 1, 1)
ChangePinName(R102, 2, 2)

# Start of element R101
ChangePinName(R101, 1, 1)
ChangePinName(R101, 2, 2)

# Start of element U101
ChangePinName(U101, 28, GPA7)
ChangePinName(U101, 27, GPA6)
ChangePinName(U101, 26, GPA5)
ChangePinName(U101, 25, GPA4)
ChangePinName(U101, 24, GPA3)
ChangePinName(U101, 23, GPA2)
ChangePinName(U101, 22, GPA1)
ChangePinName(U101, 21, GPA0)
ChangePinName(U101, 20, INTA)
ChangePinName(U101, 19, INTB)
ChangePinName(U101, 18, RESET)
ChangePinName(U101, 17, A2)
ChangePinName(U101, 16, A1)
ChangePinName(U101, 15, A0)
ChangePinName(U101, 1, GPB0)
ChangePinName(U101, 2, GPB1)
ChangePinName(U101, 3, GPB2)
ChangePinName(U101, 4, GPB3)
ChangePinName(U101, 5, GPB4)
ChangePinName(U101, 6, GPB5)
ChangePinName(U101, 7, GPB6)
ChangePinName(U101, 8, GPB7)
ChangePinName(U101, 9, VDD)
ChangePinName(U101, 10, VSS)
ChangePinName(U101, 11, NC/CS)
ChangePinName(U101, 12, SCL/SCK)
ChangePinName(U101, 13, SDA/SI)
ChangePinName(U101, 14, NC/SO)

# Start of element CONN2
ChangePinName(CONN2, 5, 5)
ChangePinName(CONN2, 3, 3)
ChangePinName(CONN2, 1, 1)
ChangePinName(CONN2, 6, 6)
ChangePinName(CONN2, 4, 4)
ChangePinName(CONN2, 2, 2)

# Start of element J3
ChangePinName(J3, 11, 11)
ChangePinName(J3, 12, 12)
ChangePinName(J3, 13, 13)
ChangePinName(J3, 14, 14)
ChangePinName(J3, 15, 15)
ChangePinName(J3, 16, 16)
ChangePinName(J3, 19, 19)
ChangePinName(J3, 17, 17)
ChangePinName(J3, 20, 20)
ChangePinName(J3, 18, 18)
ChangePinName(J3, 6, 6)
ChangePinName(J3, 7, 7)
ChangePinName(J3, 8, 8)
ChangePinName(J3, 9, 9)
ChangePinName(J3, 10, 10)
ChangePinName(J3, 5, 5)
ChangePinName(J3, 1, 1)
ChangePinName(J3, 4, 4)
ChangePinName(J3, 3, 3)
ChangePinName(J3, 2, 2)

# Start of element J2
ChangePinName(J2, 31, 31)
ChangePinName(J2, 32, 32)
ChangePinName(J2, 33, 33)
ChangePinName(J2, 34, 34)
ChangePinName(J2, 35, 35)
ChangePinName(J2, 36, 36)
ChangePinName(J2, 39, 39)
ChangePinName(J2, 37, 37)
ChangePinName(J2, 40, 40)
ChangePinName(J2, 38, 38)
ChangePinName(J2, 26, 26)
ChangePinName(J2, 27, 27)
ChangePinName(J2, 28, 28)
ChangePinName(J2, 29, 29)
ChangePinName(J2, 30, 30)
ChangePinName(J2, 25, 25)
ChangePinName(J2, 21, 21)
ChangePinName(J2, 24, 24)
ChangePinName(J2, 23, 23)
ChangePinName(J2, 22, 22)
ChangePinName(J2, 11, 11)
ChangePinName(J2, 12, 12)
ChangePinName(J2, 13, 13)
ChangePinName(J2, 14, 14)
ChangePinName(J2, 15, 15)
ChangePinName(J2, 16, 16)
ChangePinName(J2, 19, 19)
ChangePinName(J2, 17, 17)
ChangePinName(J2, 20, 20)
ChangePinName(J2, 18, 18)
ChangePinName(J2, 6, 6)
ChangePinName(J2, 7, 7)
ChangePinName(J2, 8, 8)
ChangePinName(J2, 9, 9)
ChangePinName(J2, 10, 10)
ChangePinName(J2, 5, 5)
ChangePinName(J2, 1, 1)
ChangePinName(J2, 4, 4)
ChangePinName(J2, 3, 3)
ChangePinName(J2, 2, 2)

# Start of element J1
ChangePinName(J1, 31, 31)
ChangePinName(J1, 32, 32)
ChangePinName(J1, 33, 33)
ChangePinName(J1, 34, 34)
ChangePinName(J1, 35, 35)
ChangePinName(J1, 36, 36)
ChangePinName(J1, 39, 39)
ChangePinName(J1, 37, 37)
ChangePinName(J1, 40, 40)
ChangePinName(J1, 38, 38)
ChangePinName(J1, 26, 26)
ChangePinName(J1, 27, 27)
ChangePinName(J1, 28, 28)
ChangePinName(J1, 29, 29)
ChangePinName(J1, 30, 30)
ChangePinName(J1, 25, 25)
ChangePinName(J1, 21, 21)
ChangePinName(J1, 24, 24)
ChangePinName(J1, 23, 23)
ChangePinName(J1, 22, 22)
ChangePinName(J1, 11, 11)
ChangePinName(J1, 12, 12)
ChangePinName(J1, 13, 13)
ChangePinName(J1, 14, 14)
ChangePinName(J1, 15, 15)
ChangePinName(J1, 16, 16)
ChangePinName(J1, 19, 19)
ChangePinName(J1, 17, 17)
ChangePinName(J1, 20, 20)
ChangePinName(J1, 18, 18)
ChangePinName(J1, 6, 6)
ChangePinName(J1, 7, 7)
ChangePinName(J1, 8, 8)
ChangePinName(J1, 9, 9)
ChangePinName(J1, 10, 10)
ChangePinName(J1, 5, 5)
ChangePinName(J1, 1, 1)
ChangePinName(J1, 4, 4)
ChangePinName(J1, 3, 3)
ChangePinName(J1, 2, 2)

# Start of element CONN1
ChangePinName(CONN1, 1, 1)
ChangePinName(CONN1, 4, 4)
ChangePinName(CONN1, 2, 2)
ChangePinName(CONN1, 3, 3)
